﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;


public enum GameState
{
    waitingForStart = 0,
    pause = 1,
    upgradesTime = 2,
    battleTime = 3,
    fail = 4,
    win = 5
}
public class GameManager : MonoBehaviour
{
    #region Variables

    public delegate void StartGameEvent();
    public static event StartGameEvent startGameEvent;

    public delegate void RemoveAllFightersEvent();
    public static event RemoveAllFightersEvent removeAllFightersEvent;

    public static GameState gameState;
    public Team playerTeamPublic;
    public static Team playerTeam;

    static List<Fighter> spawnedChars_friendly = new List<Fighter>(100);
    static List<Fighter> spawnedChars_enemies = new List<Fighter>(100);

    public static List<Fighter> spawnedChars = new List<Fighter>(200);

    public int wavesCount = 5;
    public static int currWave = -1;

    public float upgradesTime = 30f;
    public static float currUpgradesTimer = 0;

    public static int currGold = 0;
    public int startGold = 2000;

    #endregion

    #region GameLogic
    // Use this for initialization
	void Awake () {
        gameState = GameState.waitingForStart;

	    StartCoroutine(Loop());
	}


    public void StartGame()
    {
        Time.timeScale = 1;

        if (startGameEvent != null) startGameEvent();

        RemoveAllFighters();

        spawnedChars_enemies.Clear();
        spawnedChars_friendly.Clear();
        spawnedChars.Clear();

        //FIX ME
        List<GameObject> damagable_friendly_buildings = GameObject.FindGameObjectsWithTag("FriendlyBuilding").ToList();
        foreach (GameObject building in damagable_friendly_buildings)
        {
            if (building.GetComponent<Fighter>() != null)
            {
                spawnedChars.Add(building.GetComponent<Fighter>());
            }
        }

        gameState = GameState.upgradesTime;
        currUpgradesTimer = upgradesTime;
        playerTeam = playerTeamPublic;
        currWave = -1;
        currGold = startGold;
    }

    private IEnumerator Loop()
    {
        yield return 1;
        while (true)
        {
            if (gameState == GameState.battleTime)
            {
                spawnedChars_friendly = spawnedChars.Where(v => v.currTeam == playerTeam && v.hp != null).ToList();
                spawnedChars_enemies =
                    spawnedChars.Where(v => v.currTeam != playerTeam && v.hp != null).ToList();

                //FAIL CONDITION
                if (spawnedChars_friendly.Count == 0)
                {
                    gameState = GameState.fail;
                    Debug.Log("GAME OVER!");
                }

                //WAVE END CONDITION
                if (spawnedChars_enemies.Count == 0)
                {
                    if (currWave == wavesCount)
                    {
                        gameState = GameState.win;
                        Debug.Log("WIN!");
                    }
                    else
                    {
                        gameState = GameState.upgradesTime;
                        currUpgradesTimer = upgradesTime;

                        Debug.Log("All enemies in wave "+currWave+" died!");
                    }
                }
            }

            //TIME TO UPGRADES
            if (gameState == GameState.upgradesTime)
            {
                currUpgradesTimer -= 1f;
                
                if (currUpgradesTimer <= 0)
                {
                    gameState = GameState.battleTime;
                    
                    currWave++;

                    yield return 1;
                }
            }
            yield return new WaitForSeconds(1f);
        }
    }

    #endregion

   
    public static void RemoveFighterFromLists(GameObject go)
    {
        if (spawnedChars.Contains(go.GetComponent<Fighter>()))
        {
            spawnedChars.Remove(go.GetComponent<Fighter>());
        }
    }

    public static void RemoveAllFighters()
    {
        if (removeAllFightersEvent != null) removeAllFightersEvent();

        List<Fighter> fighters = spawnedChars.Where(v => v.hp != null /*&& v.agent != null*/).ToList();

        foreach (Fighter fighter in fighters)
        {
            Destroy(fighter.gameObject, 0.001f);
            RemoveFighterFromLists(fighter.gameObject);
        }
        //spawnedChars.Clear();
    }
}
