﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class UnitsControl : MonoBehaviour
{
    private static GameObject spawnedPointer;


	// Use this for initialization
	void Awake ()
	{
	}

    private void OnEnable()
    {
        InputManager.rightMouse += SetFighterMoveDestination;
    }
    private void OnDisable()
    {
        InputManager.rightMouse -= SetFighterMoveDestination;
    }


    public static void SetFighterMoveDestination(PointerEventData eventData)
    {
        Vector3 pos = eventData.pointerCurrentRaycast.worldPosition;
        GameObject hitGO = eventData.pointerCurrentRaycast.gameObject;
                
        //print(Selection.currSelection.Count);
        if (Selection.currSelection.Count > 0)
        {
            foreach (Selectable selectable in Selection.currSelection)
            {
                if (selectable != null)
                {
                    if (selectable.fighter.currTeam == GameManager.playerTeam)
                    {
                        selectable.fighter.SetCustomTarget(pos, hitGO.GetComponent<Fighter>());

                    }
                }
            }
        }

    }
}
