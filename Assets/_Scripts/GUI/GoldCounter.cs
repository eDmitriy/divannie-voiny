﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GoldCounter : MonoBehaviour
{
    private Text text;

	// Use this for initialization
	void Start ()
	{
	    text = GetComponent<Text>();
	    if (text == null)
	    {
	        Destroy(this);
	    }
	}
	
	// Update is called once per frame
	void FixedUpdate ()
	{
	    text.text = "Gold: " + GameManager.currGold.ToString();
	}
}
