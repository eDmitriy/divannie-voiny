﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class WavesCounter : MonoBehaviour
{
    private Text uiText;

	// Use this for initialization
	void Start ()
	{
	    uiText = GetComponent<Text>();
	}

    private void OnEnable()
    {
	    StartCoroutine(Loop());
    }

    private IEnumerator Loop()
    {
        while (true)
        {
            if (uiText == null)
            {
                yield return null;
                continue;
            }

            if (GameManager.gameState == GameState.battleTime)
            {
                uiText.text = "Wave: " + (GameManager.currWave+1);
            }
            if (GameManager.gameState == GameState.upgradesTime)
            {
                uiText.text = "Next wave " + (GameManager.currWave + 2) + " in " + GameManager.currUpgradesTimer + " sec";
            }
/*            else
            {
                uiText.text = "";
            }*/

            yield return new WaitForSeconds(0.5f);
        }
    }
}
