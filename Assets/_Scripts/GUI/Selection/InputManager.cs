﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;


public enum LeftMouseAction
{
    selectTarget,
    selectMagickTarget
}

public class InputManager : MonoBehaviour
{
    #region Variables
    public static LeftMouseAction leftMouseAction;

    //Events
    public delegate void InputManagerLeftMouse(Selectable selectable);
    public static InputManagerLeftMouse leftMouse;

    public delegate void InputManagerLeftMouseBreak(PointerEventData eventData);
    public static InputManagerLeftMouseBreak leftMouseBreak;

    public delegate void InputManagerRightMouse(PointerEventData eventData);
    public static InputManagerRightMouse rightMouse;
/*
    public delegate void PreviousMagicHotkeyPressed();
    public static PreviousMagicHotkeyPressed previousHotkeyPressEvent;*/

    #endregion

    private void OnEnable()
    {
        Selectable.selectEvent += SelectTarget;
        Selectable.commandEvent += Command;
        Selectable.selectionBreakEvent += SelectionBreak;
    }
    private void OnDisable()
    {
        Selectable.selectEvent -= SelectTarget;
        Selectable.commandEvent -= Command;
        Selectable.selectionBreakEvent -= SelectionBreak;
    }

    
    #region MouseActions
    public static void SelectTarget(Selectable s)
    {
        if (leftMouseAction == LeftMouseAction.selectTarget)
        {
            Selection.Select(s);
        }
        if (leftMouseAction == LeftMouseAction.selectMagickTarget)
        {
            if (leftMouse != null) leftMouse(s);
        }
    }

    public static void SelectionBreak(PointerEventData eventData)
    {
        if (leftMouseAction == LeftMouseAction.selectTarget)
        {
            Selection.BreakSelection(null);
        }
        if (leftMouseAction == LeftMouseAction.selectMagickTarget)
        {
            if (leftMouseBreak != null) leftMouseBreak(eventData);
        }
    }

    public static void Command(PointerEventData eventData)
    {
/*        if (leftMouseAction == LeftMouseAction.selectTarget)
        {
            if (rightMouse != null) rightMouse(eventData);
        }
        if (leftMouseAction == LeftMouseAction.selectMagickTarget)
        {
            if (leftMouseBreak != null) leftMouseBreak(eventData);
        }*/
        if (rightMouse != null) rightMouse(eventData);

    }
    #endregion
}
