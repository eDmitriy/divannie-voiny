﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.EventSystems;
using UnityEngine.UI;


public class Selection : MonoBehaviour
{
    #region Variables
    public static List<Selectable> currSelection = new List<Selectable>();
    static List<Transform> spawnedSelectionTargets = new List<Transform>();

    public Transform selectionIconParent;
    public Image selectedUnitIcon;
    public Text selectedUnitHP;
    public Text selectedUnitName;

    [Space(10)]
    public Slider lvlSlider;
    public Text lvlValue;

    [Header("Fighter Properties")] 
    public Transform selectFighterProperties_parent;
    public Text selectedFighter_attackDamage_Text;
    public Text selectedFighter_attackSpeed_Text;
    public Text selectedFighter_attackRange_Text;
    public Text selectedFighter_moveSpeed_Text;


    [Space(10)]
    public Image massSelectionImagePublic;

    [Space(10)]
    public Transform selectionTargetPrefab;
    public Transform selectionTargetParent;
    
    private static Image massSelectionImage;
    private static bool massSelectionActive = false;

    static Transform controlPanelGrid;
    static Canvas canvas;

    [Space(10)] 
    public Material glSelectionTargetsLinesMat;
    public bool useGlSelectionTargetsLines = false;
    #endregion


    private void OnEnable()
    {
        HP.fighterDeathEvent += FighterDeath;
        GameManager.startGameEvent += StartGame;
        GameManager.removeAllFightersEvent += BreakSelection;
    }
    private void OnDisable()
    {
        HP.fighterDeathEvent -= FighterDeath;
        GameManager.startGameEvent -= StartGame;
        GameManager.removeAllFightersEvent -= BreakSelection;
    }

    void Start ()
    {

	}

    private void StartGame()
    {
        BreakSelection();

        //currSelection.Clear();
        massSelectionActive = false;

        canvas = FindObjectOfType<Canvas>();

        massSelectionImage = massSelectionImagePublic;
        MassSelectionHideImage();

        controlPanelGrid = selectionIconParent.GetComponentInChildren<GridLayoutGroup>().transform;
    }


    // Update is called once per frame
	void FixedUpdate () {
	    UpdateGUI();
	}

    public void FighterDeath(Fighter fighter)
    {
        if (currSelection.Count > 0 && currSelection[0].fighter == fighter)
        {
            BreakSelection();
        }
    }

    #region GUI
    public static void Select(Selectable s)
    {
        BreakSelection();

        //SELECT ONLY FRIENLY TEAM FIGHTERS
        if (s!=null && s.fighter != null)
        {
            if (s.fighter.currTeam==GameManager.playerTeam)
            {
                currSelection.Add(s);
                ControlPanelAddButtons();
            }
        }
    }
    public static void BreakSelection(PointerEventData eventData)
    {
        ClearControlPanel();
        SetSelectionMarkerOnOff(false);

        DeleteSelectionTargets();

        currSelection.Clear();
    }
    public static void BreakSelection()
    {
        BreakSelection(null);
    }


    private void UpdateGUI()
    {
        if (currSelection.Count == 0)
        {
            selectionIconParent.gameObject.SetActive(false);
            return;
        }
        selectionIconParent.gameObject.SetActive(true);


        if (currSelection.Count > 0)
        {
            if (currSelection[0] == null)
            {
                currSelection.RemoveAt(0);
                return;
            }

            UpdateGUI_SelectionIcon();

            SetSelectionMarkerOnOff(true);

            if (spawnedSelectionTargets.Count != currSelection.Count)
            {
                DeleteSelectionTargets();
                CreateSelectionTargets();
            }
            UpdateSelectionTargets();
        }

        if (currSelection.Count > 1)
        {
            selectionIconParent.gameObject.SetActive(false);
        }
    }

    static void SetSelectionMarkerOnOff(bool val)
    {
        foreach (Selectable selectable in currSelection)
        {
            selectable.isSelected = val;
        }
    }

    private void UpdateGUI_SelectionIcon()
    {
        if (currSelection[0].texture != null)
        {
            selectedUnitIcon.sprite = currSelection[0].texture;
        }
        selectedUnitName.text = currSelection[0].gameObject.name;

        //HEALTH
        if (currSelection[0].fighter.hp != null)
        {
            HP hp = currSelection[0].fighter.hp;

            selectedUnitHP.text = "HP " +hp.healthPoints.ToString() + "/" + hp.maxHP.ToString();
        }
        else
        {
            selectedUnitHP.text = "";
        }

        //HERO LVL
        if (currSelection[0].fighter.hero != null)
        {
            lvlSlider.gameObject.SetActive(true);

            lvlSlider.maxValue = currSelection[0].fighter.hero.heroLevels[currSelection[0].fighter.hero.currLevel].xpToNextLevel;
            lvlSlider.value = currSelection[0].fighter.hero.currXPAmount;

            lvlValue.text = (currSelection[0].fighter.hero.currLevel+1).ToString();
        }
        else
        {
            lvlSlider.gameObject.SetActive(false);
        }

        //PROPERTIES

        if (currSelection[0].fighter.agent != null && currSelection[0].fighter.weapon!=null)
        {
            if (selectFighterProperties_parent != null) selectFighterProperties_parent.gameObject.SetActive(true);

            if (selectedFighter_attackDamage_Text != null)
                selectedFighter_attackDamage_Text.text = currSelection[0].fighter.weapon.AttackDamage.ToString();
            if (selectedFighter_attackSpeed_Text != null)
                selectedFighter_attackSpeed_Text.text = currSelection[0].fighter.weapon.AttackSpeed.ToString();
            if (selectedFighter_attackRange_Text != null)
                selectedFighter_attackRange_Text.text = currSelection[0].fighter.weapon.AttackRadius.ToString();

            if (selectedFighter_moveSpeed_Text != null)
                selectedFighter_moveSpeed_Text.text = currSelection[0].fighter.maxMoveSpeed.ToString();
        }
        else
        {
            if (selectFighterProperties_parent != null) selectFighterProperties_parent.gameObject.SetActive(false);
        }
    }


    static void ClearControlPanel()
    {
        if (currSelection.Count > 0)
        {
            if (currSelection[0]!=null && currSelection[0].GetComponent<ControlPanelActions>() != null)
            {
                currSelection[0].GetComponent<ControlPanelActions>().RemoveAllButtonsFromControlPanel();
            }
        }
    }
    static void ControlPanelAddButtons()
    {
        if (currSelection[0] != null && currSelection[0].GetComponent<ControlPanelActions>() != null)
        {
            currSelection[0].GetComponent<ControlPanelActions>().AddButtonsToControlPanel(controlPanelGrid);
        }
    }
    #endregion

    #region MassSelection

    public static void MassSelectionUpdate(Vector2 startPoint, Vector2 endPoint)
    {
        if (massSelectionImage == null)
        {
            return;
        }
        massSelectionActive = true;

        massSelectionImage.transform.position = Vector3.Lerp(endPoint, startPoint, 0.5f);
        massSelectionImage.rectTransform.sizeDelta = new Vector3(
            Mathf.Abs(endPoint.x - startPoint.x) / canvas.scaleFactor,
            Mathf.Abs(endPoint.y - startPoint.y) / canvas.scaleFactor,
            1);
        
    }

    public static void MassSelectionEnd()
    {
        if (massSelectionImage == null || !massSelectionActive)
        {
            return;
        }
        BreakSelection();

        Camera mainCamera = Camera.main;
        List<Fighter> selectedFighters =
            GameManager.spawnedChars.Where(
            v => (v!=null
                    & v.currTeam == GameManager.playerTeam
                    && IsInsideRect(
                        massSelectionImage.transform.position,
                        massSelectionImage.rectTransform.sizeDelta,
                        mainCamera.WorldToScreenPoint(v.transform.position))
                    && v.hp != null
                    && v.weapon != null
                    && v.agent != null)
            ).ToList();
        
        //FIX ME
        foreach (Fighter fighter in selectedFighters)
        {
            if (fighter.GetComponent<Selectable>() != null)
            {
                currSelection.Add(fighter.GetComponent<Selectable>());
            }
        }
        MassSelectionHideImage();
    }

    private static void MassSelectionHideImage()
    {
        if (massSelectionImage == null)
        {
            return;
        }
        massSelectionActive = false;

        massSelectionImage.transform.position = Vector3.zero;
        massSelectionImage.rectTransform.sizeDelta = Vector2.zero;
    }

    static bool IsInsideRect(Vector2 rectCenter, Vector2 rectSize, Vector2 screenPoint)
    {
        return screenPoint.x > rectCenter.x - Mathf.Abs(rectSize.x)/2
                 && screenPoint.x < rectCenter.x + Mathf.Abs(rectSize.x)/2
                 && screenPoint.y > rectCenter.y - Mathf.Abs(rectSize.y)/2
                 && screenPoint.y < rectCenter.y + Mathf.Abs(rectSize.y)/2;
    }

    void OnApplicationFocus(bool focusStatus)
    {
        MassSelectionHideImage();
    }

    #endregion

    #region SelectionTargets

    void CreateSelectionTargets()
    {
        for (int i = 0; i < currSelection.Count; i++)
        {
            //if (currSelection[i].fighter == null){continue;}

            Transform go = Instantiate(selectionTargetPrefab) as Transform;
            go.SetParent(selectionTargetParent);
            go.transform.position = Vector3.down*-9000f;
            go.localScale = Vector3.one;

            spawnedSelectionTargets.Add(go);
        }
    }

    static void UpdateSelectionTargets()
    {
        Camera mainCamera = Camera.main;

        for (int i = 0; i < currSelection.Count; i++)
        {
            if (currSelection[i].fighter == null)
            {
                spawnedSelectionTargets[i].position = Vector3.down*9000;
                continue;
            }

            if (spawnedSelectionTargets[i] != null && currSelection[i].fighter.agent!=null)
            {
                spawnedSelectionTargets[i].position = mainCamera.WorldToScreenPoint(currSelection[i].fighter.movePos+Vector3.up*3f);
            }
        }
    }

    static void DeleteSelectionTargets()
    {
        for (int i = 0; i < spawnedSelectionTargets.Count; i++)
        {
            if(spawnedSelectionTargets[i]!=null) Destroy(spawnedSelectionTargets[i].gameObject);
        }
        spawnedSelectionTargets.Clear();
    }

    #endregion

    #region GL_selection_Lines
    void OnPostRender()
    {
        if (!glSelectionTargetsLinesMat)
        {
            Debug.LogError("Please Assign a material on the inspector");
            return;
        }
        if (!useGlSelectionTargetsLines){return;}

        GL.PushMatrix();
        glSelectionTargetsLinesMat.SetPass(0);
        GL.LoadOrtho();
        GL.Begin(GL.LINES);
        GL.Color(Color.white);

        Vector2 pos;
        for (int i = 0; i < currSelection.Count; i++)
        {
            if (currSelection[i] == null) continue;
            if (currSelection[i].fighter == null) continue;
            if (currSelection[i].fighter.moveTarget == null) continue;   
 
            pos = Camera.main.WorldToScreenPoint(currSelection[i].fighter.moveTarget.transform.position);
            pos.x /= Screen.width;
            pos.y /= Screen.height;
            GL.Vertex( pos );

            pos = Camera.main.WorldToScreenPoint(currSelection[i].transform.position);
            pos.x /= Screen.width;
            pos.y /= Screen.height;
            GL.Vertex(pos);

            //GL.Vertex(new Vector2(0,0));
            //GL.Vertex(new Vector2(Screen.width,Random.Range(0,Screen.height)));
                
            
        }

        GL.End();
        GL.PopMatrix();
    }
    #endregion


}
