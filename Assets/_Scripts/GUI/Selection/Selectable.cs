﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class Selectable : MonoBehaviour,IPointerDownHandler, IPointerUpHandler, IDragHandler
{
    #region Variables
    public Sprite texture;
    public GameObject selectionObject;
    public bool breakSelection = false;

[HideInInspector]
    public bool isSelected = false;
    private bool isSelectedTemp = true;

[HideInInspector]
    public Fighter fighter;

    private bool applicationIsFocused = true;

    //Events
    public delegate void Select(Selectable selectable);
    public static Select selectEvent;

    public delegate void SelectionBreak(PointerEventData eventData);
    public static SelectionBreak selectionBreakEvent;

    public delegate void Command(PointerEventData eventData);
    public static Command commandEvent;
    #endregion


    private void Awake()
    {
        fighter = GetComponent<Fighter>();
    }

    private void FixedUpdate()
    {
        if (isSelected != isSelectedTemp)
        {
            if (selectionObject != null) selectionObject.SetActive(isSelected);

            isSelectedTemp = isSelected;
        }
    }

    #region Input
    public void OnPointerDown(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Left)
        {
            if (!breakSelection)
            {
               //Selection.Select(this);
                
                if (selectEvent != null) selectEvent(this);
            }
            else
            {
                if (selectionBreakEvent != null) selectionBreakEvent(eventData);
                //Selection.BreakSelection();
            }
        }

        if (eventData.button == PointerEventData.InputButton.Right)
        {
            if (commandEvent != null) commandEvent(eventData);
        }
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (!applicationIsFocused)
        {
            eventData.pointerDrag = null;
            return;
        }

        if (eventData.button == PointerEventData.InputButton.Left && eventData.delta.magnitude>2f)
        {
            Selection.MassSelectionUpdate(eventData.pointerPressRaycast.screenPosition, eventData.position);
        }
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Left)
        {
            Selection.MassSelectionEnd();
        }
    }

    void OnApplicationFocus(bool focusStatus)
    {
        applicationIsFocused = focusStatus;
    }
    #endregion


}
