﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.Events;

public class ControlPanelActions : MonoBehaviour
{
    public Transform buttonPrefab;
    public int currActionsListNumber = 0;

    public ControlPanelActionsList[] controlPanelActionsLists = new ControlPanelActionsList[0];
    List<ControlPanelButton> spawnedButtons = new List<ControlPanelButton>();

    private Transform buttonsParentLast;


    public void AddButtonsToControlPanel(Transform parent)
    {
        if (spawnedButtons.Count > 0 || (controlPanelActionsLists.Length-1) < currActionsListNumber) return;
        buttonsParentLast = parent;

        ControlPanelAction[] list = controlPanelActionsLists[currActionsListNumber].controlPanelActions;
        if (list.Length == 0) return;

        for (int i = 0; i < list.Length; i++)
        {
            Transform go = Instantiate(buttonPrefab) as Transform;
            if (go != null)
            {
                go.SetParent(parent);
                go.name = list[i].action.GetType().ToString();

                var i1 = i;
                go.GetComponent<Button>().onClick.AddListener(() => { list[i1].CallAction(); });

                go.localScale = Vector3.one;

                spawnedButtons.Add(go.GetComponent<ControlPanelButton>());

                if (list[i].guiIcon != null) spawnedButtons[i].icon.sprite = list[i].guiIcon;
                spawnedButtons[i].currButtonType = list[i].buttonType;

                if (list[i].buttonInfoObject != null && list[i].buttonInfoObject.GetComponent<IControlPanelButtonInfo>() != null)
                {
                    spawnedButtons[i].buttonInfoParent = list[i].buttonInfoObject.GetComponent<IControlPanelButtonInfo>();
                }
            }
        }
    }

    public void RemoveAllButtonsFromControlPanel()
    {
        for (int i = 0; i < spawnedButtons.Count; i++)
        {
            spawnedButtons[i].GetComponent<Button>().onClick.RemoveAllListeners();
            Destroy(spawnedButtons[i].gameObject);
        }
        spawnedButtons.Clear();
    }

    public void UpdateButtons()
    {
        if(buttonsParentLast==null) return;

        RemoveAllButtonsFromControlPanel();
        AddButtonsToControlPanel(buttonsParentLast);
    }
}
