﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public enum ControlPanelButtonType
{
    upgrade,
    levelUP,
    trainMinion,
    magic
}
public class ControlPanelButton : MonoBehaviour
{
    public ControlPanelButtonType currButtonType;

    public float cooldownTimer = 0;

    public Image cooldownImage;
    public Image icon;

    public Text cost;
    public Text orderCountText;

    private bool canUpgrade = true;

    public Transform upgradeImage;

   // public KeyCode hotkey;

[HideInInspector]public IControlPanelButtonInfo buttonInfoParent;



	// Use this for initialization
	void Start ()
	{
	    if (currButtonType == ControlPanelButtonType.upgrade)
	    {
	        cooldownImage.enabled = false;
	        orderCountText.enabled = false;
	    }

	    StartCoroutine(Loop());
	}
/*
    private void Update()
    {
        if (Input.GetKeyDown(hotkey))
        {
            GetComponent<Button>().onClick.Invoke();
        }
    }*/


    private IEnumerator Loop()
    {
        while (true)
        {
            if (buttonInfoParent != null)
            {
                //upgradeButton
                canUpgrade = buttonInfoParent.IsUpgradeOnMaxLevel;
                if (currButtonType == ControlPanelButtonType.upgrade)
                {
                    if (upgradeImage != null) upgradeImage.gameObject.SetActive(canUpgrade);
                    gameObject.SetActive(canUpgrade);
                }
                else
                {
                    upgradeImage.gameObject.SetActive(false);
                }

                //info
                cost.text = buttonInfoParent.Cost>0? buttonInfoParent.Cost.ToString() : "";
                cooldownTimer = buttonInfoParent.Cooldown;
                orderCountText.text = buttonInfoParent.OrderCount>0? buttonInfoParent.OrderCount.ToString() : "";
            }

            cooldownImage.fillAmount = cooldownTimer;

            yield return 5;
        }
    }


}
