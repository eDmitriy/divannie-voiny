﻿using UnityEngine;
using System.Collections;

public interface IControlPanelButtonInfo{

    int Cost { get; }
    float Cooldown { get; }

    int OrderCount { get; }

    bool IsUpgradeOnMaxLevel { get; }
}
