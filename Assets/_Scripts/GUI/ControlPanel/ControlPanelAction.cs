﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.Events;

[Serializable]
public class ControlPanelAction
{
    public ControlPanelButtonType buttonType;

    public string name = "";

    public Sprite guiIcon;

    public GameObject buttonInfoObject;

    [Space(10)]
    public UnityEvent action;

    

    public void CallAction()
    {
        action.Invoke();
    }
}

[Serializable]
public class ControlPanelActionsList
{
    public ControlPanelAction[] controlPanelActions = new ControlPanelAction[1];
}
