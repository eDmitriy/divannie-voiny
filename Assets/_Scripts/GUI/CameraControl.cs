﻿using UnityEngine;
using System.Collections;

public enum CameraState
{
    free,
    focusHero
}
public class CameraControl : MonoBehaviour
{
    #region Variables
    public CameraState cameraState;

    [Header("Free Camera")]
    public float freeMoveSpeed = 1f;
    public float sensetivityPercentage = 0.1f;

    public Bounds cameraMoveBounds;

    [Header("Focus Hero")] 
    public KeyCode hotkey = KeyCode.Space;
    public float focusedMoveSpeed = 50f;
    public Fighter hero;

    [Header("ScrollWheel")] 
    public float scrollWheelSpeed = 10f;
    
    private float cameraStartHeight;

    private Camera mainCamera;
    private Vector3 cameraNewPosition;
    private Vector3 moveDirection;
    #endregion

    // Use this for initialization
	void Awake ()
	{
	    mainCamera = GetComponent<Camera>();
	    cameraStartHeight = mainCamera.transform.position.y;
	}

    private void OnEnable()
    {
        Nexus.heroSpawned_event += FocusFighter;
        HP.fighterDeathEvent += CancelFocusOnHeroDeath;
    }

    private void OnDisable()
    {
        Nexus.heroSpawned_event -= FocusFighter;
        HP.fighterDeathEvent -= CancelFocusOnHeroDeath;
    }

    // Update is called once per frame
	void LateUpdate ()
	{
	    if (mainCamera == null) return;

	    if (GameManager.gameState == GameState.upgradesTime || GameManager.gameState == GameState.battleTime)
	    {
	        cameraStartHeight -= Input.GetAxis("Mouse ScrollWheel")*scrollWheelSpeed;

            Hotkeys();

            if (hero == null)
            {
                cameraState = CameraState.free;
            }

	        if (cameraState == CameraState.free)
	        {
	            FreeCamera();
	        }
            if (cameraState == CameraState.focusHero)
            {
                FocusedCamera();
            }
	    }
	}

    private void Hotkeys()
    {
        if (Input.GetKeyDown(hotkey))
        {
            if (cameraState == CameraState.focusHero)
            {
                cameraState = CameraState.free;
                return;
            }
            if (cameraState == CameraState.free && hero != null)
            {
                FocusFighter(hero);
                return;
            }
        }
    }

    #region CameraAnimations 
    private void FreeCamera()
    {
        if (IsMouseOverActiveZone())
        {
            //MOVE DIRECTION
            moveDirection = Input.mousePosition - new Vector3(Screen.width / 2f, Screen.height / 2f);
            moveDirection.z = moveDirection.y;
            moveDirection.y = 0;
            moveDirection.Normalize();

            //CALC NEW CAMERA POSITION=
            cameraNewPosition = Vector3.Lerp(
                mainCamera.transform.position,
                mainCamera.transform.position + moveDirection * freeMoveSpeed,
                Time.deltaTime * freeMoveSpeed);
        }

        cameraNewPosition.y = cameraStartHeight;

        //IF INSIDE BOUNDS
        if (cameraMoveBounds.Contains(cameraNewPosition))
        {
            mainCamera.transform.position = cameraNewPosition;

        }
    }

    private void FocusedCamera()
    {
        //Pythagorean theorem
        float distTemp = 0;
        cameraNewPosition = hero.transform.position;
        cameraNewPosition.y = mainCamera.transform.position.y;
        distTemp = Vector3.Distance(cameraNewPosition, mainCamera.transform.position);
        distTemp = Mathf.Sqrt( Mathf.Pow( distTemp, 2 ) + Mathf.Pow( cameraStartHeight, 2) );

        cameraNewPosition = hero.transform.position + (mainCamera.transform.rotation*(Vector3.forward * -distTemp));
        
        mainCamera.transform.position = Vector3.Lerp(
            mainCamera.transform.position, 
            cameraNewPosition, 
            Time.deltaTime * focusedMoveSpeed);
    }

    private bool IsMouseOverActiveZone()
    {
        return Input.mousePosition.x > Screen.width*(1 - sensetivityPercentage)
               || Input.mousePosition.x < Screen.width*sensetivityPercentage
               || Input.mousePosition.y > Screen.height*(1 - sensetivityPercentage)
               || Input.mousePosition.y < Screen.height*sensetivityPercentage;
    }
    #endregion


    public void FocusFighter(Fighter fighter)
    {
        //print("Cameracontrol : Focus fighter "+fighter);
        hero = fighter;

        cameraState = CameraState.focusHero;
    }

    public void CancelFocusOnHeroDeath(Fighter deadFighter)
    {
        if (hero != null && deadFighter == hero)
        {
            hero = null;
            cameraState = CameraState.free;
        }

    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.white;
        Gizmos.DrawWireCube(cameraMoveBounds.center, cameraMoveBounds.size);
    }
}
