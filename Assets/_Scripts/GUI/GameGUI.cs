﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class GameGUI : MonoBehaviour
{
    public Transform[] folders = new Transform[6]; //6 gameStates     
   /* waitingForStart = 0,
    pause = 1,
    upgradesTime = 2,
    battleTime = 3,
    fail = 4,
    win = 5*/

    GameState gameStateTemp = GameState.win;
    private GameState gameStateBeforePause;


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void FixedUpdate () {
	    if (gameStateTemp != GameManager.gameState)
	    {
            UpdateMenus();
	    }
	}

    private void UpdateMenus()
    {
        gameStateTemp = GameManager.gameState;

/*        for (int i = 0; i < folders.Length; i++)
        {
            if (folders[i] == null) continue;

            folders[i].gameObject.SetActive(i == (int)gameStateTemp);
        }*/
        folders[0].gameObject.SetActive(gameStateTemp == GameState.waitingForStart);
        folders[1].gameObject.SetActive(gameStateTemp == GameState.pause);
        folders[2].gameObject.SetActive(gameStateTemp == GameState.upgradesTime || gameStateTemp == GameState.battleTime);
        folders[3].gameObject.SetActive(gameStateTemp == GameState.upgradesTime || gameStateTemp == GameState.battleTime);
        folders[4].gameObject.SetActive(gameStateTemp == GameState.fail);
        folders[5].gameObject.SetActive(gameStateTemp == GameState.win);
    }

    public void Pause()
    {
        if (GameManager.gameState == GameState.pause)
        {
            Time.timeScale = 1;
            GameManager.gameState = gameStateBeforePause;
            return;
        }

        if (GameManager.gameState != GameState.pause)
        {
            gameStateBeforePause = GameManager.gameState;
            GameManager.gameState = GameState.pause;
            UpdateMenus();

            Time.timeScale = 0.001f;
            return;
        }
    }

    public void MainMenu()
    {
        Time.timeScale = 1;

        GameManager.gameState = GameState.waitingForStart;
        GameManager.RemoveAllFighters();

    }

}
