﻿using System;
using UnityEngine;
using System.Collections;

[Serializable]
public class BarrakUpgrades
{
    [Header("Barrak properties")]
    [Range(0, 99999)]
    public int upgradeCost = 0;
    [Range(0, 600f)]
    public float trainSpeed = 1f;

    [Header("Fighter Properties")]
    public FighterProperties fighterProperties;
}
