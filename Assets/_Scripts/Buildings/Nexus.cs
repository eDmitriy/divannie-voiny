﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using UnityEngine.UI;

public class Nexus : MonoBehaviour
{
    public delegate void HeroSpawned(Fighter fighter);
    public static HeroSpawned heroSpawned_event;

    public Fighter heroPrefab;
    public Fighter spawnedHero;

    [Space(10)]
    public float currRespawnWaitTime = 0;
    public float respawnWaitTimePerHeroLevel = 10;

    [Header("GUI resurrection time left ")]
    public Text text_resurrectionTimeLeft ;

    public static Transform thisTransform;

    private Spawner spawner;



    private void OnEnable()
    {
        HP.fighterDeathEvent += HeroDead;
        GameManager.startGameEvent += StartGame;
    }

    private void OnDisable()
    {
        HP.fighterDeathEvent -= HeroDead;
        GameManager.startGameEvent -= StartGame;
    }



    // Use this for initialization
	void Start ()
	{
	    thisTransform = transform;

	    spawner = GetComponentInChildren<Spawner>();

	    StartCoroutine(NexusLoop());
	}

    private void StartGame()
    {
        spawnedHero = null;
        currRespawnWaitTime = 0;
    }

    private IEnumerator NexusLoop()
    {
        yield return 5;
        while (true)
        {
            if (GameManager.gameState == GameState.battleTime || GameManager.gameState == GameState.upgradesTime)
            {
                if (currRespawnWaitTime < 0.01f)
                {
                    if (spawnedHero == null)
                    {
                        SpawnHero();
                    }

                    if (spawnedHero != null && !spawnedHero.isActiveAndEnabled)
                    {
                        SpawnHero();
                    }
                }

                currRespawnWaitTime = Mathf.Clamp(currRespawnWaitTime - 0.1f, 0, float.MaxValue);

                text_resurrectionTimeLeft.text = currRespawnWaitTime > 1f ?
                    "Hero resurrection.\nTime left: " + (int)(currRespawnWaitTime / 60) + " : " +
                    (int)(currRespawnWaitTime % 60)
                    : "";
            }


            yield return new WaitForSeconds(0.1f);
        }
    }

    public void HeroDead(Fighter deadFighter)
    {
        if (deadFighter == spawnedHero)
        {
            currRespawnWaitTime += (spawnedHero.hero.currLevel+1)*respawnWaitTimePerHeroLevel;
        }
    }

    private void SpawnHero()
    {
        if (spawnedHero == null)
        {
            spawnedHero = spawner.SpawnFighter(1, heroPrefab.transform)[0];
        }
        else
        {
            spawnedHero.transform.position = spawner.SpawnPos();
            GameManager.spawnedChars.Add(spawnedHero);
            spawnedHero.gameObject.SetActive(true);
            spawnedHero.hero.Upgrade(0);
            spawnedHero.hp.Heal(spawnedHero.hp.maxHP);
        }

        Selection.Select(spawnedHero.GetComponent<Selectable>());

        if (heroSpawned_event != null) heroSpawned_event(spawnedHero);
    }
}
