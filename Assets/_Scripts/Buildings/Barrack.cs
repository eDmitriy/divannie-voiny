﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;


public class Barrack : MonoBehaviour,IControlPanelButtonInfo
{
    #region Variables
    public Team currTeam;
    
    public Transform minionPrefab;
    public int currentUpgradeLevel = 0;
    int maxUpgradeLevel = 5;
    
    public float minionTrainTimer = 0;
    private const float minionTrainTimerUpdateSubsteps = 20;

    List<Transform> trainOrder = new List<Transform>();

    [Header("Upgrades")]
    public BarrakUpgrades[] upgradeLevels = new BarrakUpgrades[5];

    Spawner spawner;
    #endregion

    void Start ()
    {
	    spawner = GetComponentInChildren<Spawner>();

	    StartCoroutine(Loop());
	}


    private void OnEnable()
    {
        GameManager.startGameEvent += StartGame;
    }

    private void OnDisable()
    {
        GameManager.startGameEvent -= StartGame;
    }

    public void StartGame()
    {
        maxUpgradeLevel = upgradeLevels.Length;
        currentUpgradeLevel = 0;

        minionTrainTimer = 0;
        trainOrder.Clear();
    }

    //Minions training order
    private IEnumerator Loop()
    {
        while (true)
        {
            minionTrainTimer = Mathf.Clamp(minionTrainTimer - 1f/minionTrainTimerUpdateSubsteps, 0f, 1);

            if (minionTrainTimer < 0.01f && trainOrder.Count>0)
            {
                Fighter[] spawnedFighters = spawner.SpawnFighter(1, trainOrder[0]);
                if (spawnedFighters.Length>0)
                {
                    yield return 1;
                    SetUpgradesToMinion(spawnedFighters, upgradeLevels[currentUpgradeLevel].fighterProperties);
                }
                trainOrder.RemoveAt(0);

                if (trainOrder.Count > 0)
                {
                    minionTrainTimer += 1;
                }
            }
            yield return new WaitForSeconds((1f / upgradeLevels[currentUpgradeLevel].trainSpeed)/minionTrainTimerUpdateSubsteps);
        }
    }


    #region Upgrades
    public void SetUpgradesToMinionAll(FighterProperties properties)
    {
        List<Fighter> spawnedMinions =
            GameManager.spawnedChars.Where(v => v.currTeam == currTeam && v.spawnerObj == transform.root.gameObject)
                .ToList();

        SetUpgradesToMinion(spawnedMinions.ToArray(), properties);
    }

    public static void SetUpgradesToMinion(Fighter[] fighters, FighterProperties properties)
    {
        foreach (Fighter fighter in fighters)
        {
            if (fighter == null) continue;

            fighter.maxMoveSpeed = properties.moveSpeed;

            if (fighter.hp != null)
            {

                fighter.hp.maxHP = properties.maxHP;
                fighter.hp.Heal(0);

                fighter.hp.armor = properties.armor;

                fighter.hp.goldReward = properties.goldReward;
                fighter.hp.xpReward = properties.xpReward;
            }
            if (fighter.weapon != null)
            {
                fighter.weapon.AttackDamage = properties.attackDamage;
                fighter.weapon.AttackSpeed = properties.attackSpeed;
                fighter.weapon.AttackRadius = properties.attackRange;
            }
        }

    }
    #endregion

    #region Actions
    public void Train()
    {
        if (GameManager.currGold >= upgradeLevels[currentUpgradeLevel].fighterProperties.unitCost)
        {
            GameManager.currGold -= upgradeLevels[currentUpgradeLevel].fighterProperties.unitCost;
            trainOrder.Add(minionPrefab);

            if (trainOrder.Count == 1)
            {
                minionTrainTimer += 1 / upgradeLevels[currentUpgradeLevel].trainSpeed;
            }
        }
    }
    public void Upgrade()
    {
        if (currentUpgradeLevel == maxUpgradeLevel-1)
        {
            return;
        }
        if (GameManager.currGold >= upgradeLevels[currentUpgradeLevel + 1].upgradeCost)
        {
            GameManager.currGold -= upgradeLevels[currentUpgradeLevel + 1].upgradeCost;
            currentUpgradeLevel = Mathf.Clamp(currentUpgradeLevel+1,0, maxUpgradeLevel);

            SetUpgradesToMinionAll(upgradeLevels[currentUpgradeLevel].fighterProperties);
        }
    }

    #endregion

    #region IControlPanelButtonInfo
    public int Cost
    {
        get { return upgradeLevels[currentUpgradeLevel].fighterProperties.unitCost; }
    }

    public float Cooldown
    {
        get { return minionTrainTimer; }
    }

    public int OrderCount
    {
        get { return trainOrder.Count; }
    }

    public bool IsUpgradeOnMaxLevel
    {
        get { return currentUpgradeLevel < maxUpgradeLevel - 1; }
    }

    #endregion
}
