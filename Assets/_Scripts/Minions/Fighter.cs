﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public enum Team
{
    team1,
    team2,
    team3
}

public class Fighter : MonoBehaviour
{
    #region Variables
    public Team currTeam;

    [HideInInspector] public GameObject spawnerObj;

    public float maxMoveSpeed = 20f;
[HideInInspector] 
    public List<float> moveSpeedEffects = new List<float>();

    public bool autoTarget = true;

    [HideInInspector] public HP hp;
    [HideInInspector] public NavMeshAgent agent;
    [HideInInspector] public IWeapon weapon;
    [HideInInspector] public Hero hero;

    [HideInInspector] public Fighter moveTarget;
    [HideInInspector] public Fighter attackTarget;

    [HideInInspector] public Vector3 movePos;
    CustomTarget customtarget;

    #endregion

    void Start ()
	{
        movePos = Vector3.down * 9000;
	}

    private void OnEnable()
    {
        weapon = GetComponent<IWeapon>();
        agent = GetComponent<NavMeshAgent>();
        hp = GetComponent<HP>();
        hero = GetComponent<Hero>();

        if (agent != null || weapon != null)
        {
            if (!autoTarget)
            {
                SetCustomTarget(transform.position, null);
            }

            StartCoroutine(Loop());
        }
    }

    //AI LOOP
    private IEnumerator Loop()
    {
        //CALL LOGIC AT DIFFERENT TIME, EXPECTING LOWER CPU USAGE
        yield return new WaitForSeconds(Random.Range(0, 0.2f));

        while (true)
        {
            if (GameManager.gameState == GameState.battleTime || GameManager.gameState == GameState.upgradesTime)
            {
                //IF NO MANUAL TARGETS
                if (customtarget==null)
                {
                    if (autoTarget)
                    {
                        //FIND CLOSEST ENEMY AND CALCULATE MOVE POINT FOR IT
                        moveTarget = SelectClosestEnemy();
                        /*//FIX ME - if no enemies and team == playerTeam then go to heal 
                        if (moveTarget == this && currTeam == GameManager.playerTeam)
                        {
                            moveTarget = HealingFountain.thisFighter;
                        }*/
                        CalculateMoveDestination(moveTarget);
                    }
                }
                else//IF MANUAL TARGET EXISTS
                {
                    if (customtarget.fighterTarget != null)
                    {
                        CalculateMoveDestination(customtarget.fighterTarget);
                        moveTarget = customtarget.fighterTarget;
                    }
                    else
                    {
                        movePos = customtarget.movePosition;
                    }

                    if (customtarget.TargetIsReached(transform.position, 5f*transform.localScale.magnitude))
                    {
                        customtarget = null;
                        
                        yield return null;
                        continue;
                    }
                }


                //SET DESTINATION FOR PATHFINDING
                if (Vector3.Distance(transform.position, movePos) > 0f
                    //&& moveTarget.gameObject!=gameObject
                    && agent != null && agent.enabled
                    )
                {
                    agent.speed = maxMoveSpeed * Random.Range(0.9f, 1.1f)*CalcMoveSpeedEffects();
                    agent.SetDestination(movePos);
                }


                //IF HAVE WEAPON AND CAN ATTACK THEN STOP AND ATTACK ELSE MOVE TO ENEMY
                if (moveTarget != null)
                {
                    if (moveTarget.gameObject != gameObject && moveTarget.currTeam != currTeam && weapon != null)
                    {
                        attackTarget = moveTarget;

                        if (weapon.InAttackRadius(movePos))
                        {
                            weapon.Attack(attackTarget, movePos);

                            if (agent != null) agent.speed = 0;
                        }
                    }
                }
            }
            
            yield return new WaitForSeconds(0.3f);
        }
    }

    private float CalcMoveSpeedEffects()
    {
        float value = 0;
        foreach (float f in moveSpeedEffects)
        {
            value = Mathf.Clamp(value + f, -1, 1);
        }
        return  1f - value;
    }

    #region FighterLogic

    List<Fighter> movingEnemies = new List<Fighter>(100);
    List<Fighter> staticEnemies = new List<Fighter>(100);
    Fighter SelectClosestEnemy()
    {
        Fighter moveTarget = this;
        //Get all movable fighters with diffirent team
        movingEnemies = GameManager.spawnedChars.Where(v => v.currTeam != currTeam
            && v.hp != null && v.weapon != null).ToList();

        //if there is no movable enemies then attack buildings
        if (movingEnemies.Count == 0)
        {
            staticEnemies = GameManager.spawnedChars.Where(v => v.currTeam != currTeam
                && v.hp != null && v.agent==null).ToList();
            moveTarget = GetClosestTarget(ref staticEnemies, this);
        }
        else
        {
            moveTarget = GetClosestTarget(ref movingEnemies, this);
        }
        return moveTarget;
    }


    float distanceTemp = float.MaxValue;
    float currDistance = float.MaxValue;
    Fighter GetClosestTarget(ref List<Fighter> list, Fighter minionInstance)
    {
        distanceTemp = float.MaxValue;
        currDistance = float.MaxValue;
        Fighter closestFighterObject = minionInstance;

        for (int i = 0; i < list.Count; i++)
        {
            if (list[i] == null)
            {
                continue;
            }
            currDistance = Vector3.Distance(minionInstance.transform.position, list[i].transform.position);
            if (currDistance < distanceTemp)
            {
                distanceTemp = currDistance;
                closestFighterObject = list[i];
            }
        }
        //Debug.Log("Enemy:Closest target = "+closestGameObject.name);
        return closestFighterObject;
    }

    public void CalculateMoveDestination(Fighter target)
    {
        //Set pathfinding destination to targets closest collider point + some random shift
        if (target.agent == null)
        {
            movePos = Vector3.Lerp(
                target.GetComponent<Collider>().ClosestPointOnBounds(transform.position),
                target.transform.position,
                Random.Range(0f, 1.0f)
                );
        }
        else
        {
            movePos = target.transform.position;
        }
    }
    #endregion

    #region FighterPublicActions
    public void SetCustomTarget(Vector3 movePosition, Fighter customTarget)
    {
        //customAttackTarget = customTarget;
        moveTarget = customTarget;

        movePos = movePosition;
        //manualTargetReached = false;

        customtarget = new CustomTarget();
        customtarget.NewTarget(customTarget, movePosition);
    }

    public void CancelCustomTarget()
    {
        //customAttackTarget = null;
        moveTarget = null;

        //manualTargetReached = true;

        customtarget = null;
    }
    #endregion
}
