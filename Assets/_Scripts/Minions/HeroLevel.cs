﻿using System;
using UnityEngine;


[Serializable]
public class HeroLevel
{
    [Range(0, 99999)] public int xpToNextLevel = 500;
    [Range(0, 100)] public int addSkillPoints = 1;

    public FighterProperties heroPropertiesOnNewLevel = new FighterProperties();
}
