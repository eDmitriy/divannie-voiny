﻿using System;
using UnityEngine;
using System.Collections;

[Serializable]
public class FighterProperties
{
/*    [Range(0, 99999)] public int upgradeCost = 0;
    [Range(0, 600f)] public float trainSpeed = 1f;*/


    [Header("Unit properties")]
    [Range(0, 99999)] public int unitCost = 100;
    [Space(10)]
    [Range(0, 99999)] public int maxHP = 100;
    [Range(0, 1f)] public float armor = 0f;
    [Space(10)]
    [Range(0, 9999)] public int attackDamage = 20;
    [Range(0, 100f)] public float attackSpeed = 1f;
    [Range(0, 9999)] public float attackRange = 3f;
    [Space(10)]
    [Range(0, 500)] public float moveSpeed = 20f;
    [Space(10)]
    [Range(0, 9999)] public int goldReward = 10;
    [Range(0, 9999)] public int xpReward = 10;
}
