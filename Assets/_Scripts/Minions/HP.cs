﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Fighter))]
public class HP : MonoBehaviour
{
    public delegate void FighterDeath(Fighter fighter);
    public static FighterDeath fighterDeathEvent;

    public int healthPoints;
    public int maxHP = 100;

[Range(0,1)] public float armor = 0;//0-1

    public int goldReward = 0;
    public int xpReward = 0;

    private Fighter parentfighter;

	IEnumerator Start ()
	{
	    yield return 5;
	    parentfighter = GetComponent<Fighter>();
	    healthPoints = maxHP;
	}


    public void Damage(int value, Fighter damageDealer)
    {
        if (healthPoints <=0) return;
        
        healthPoints -= (int)((float)value*(Mathf.Clamp(1f-armor, 0f ,1f)));

        if (healthPoints <= 0)
        {
            Death(damageDealer);
        }
    }
    public void Heal(int value)
    {
        healthPoints = Mathf.Clamp(healthPoints += value, 0, maxHP);
    }

    public void Death(Fighter damageDealer)
    {
        //Debug.Log(damageDealer.gameObject.name+" Killed "+gameObject.name);
        GameManager.RemoveFighterFromLists(this.gameObject);
        GetReward(damageDealer);


        if (fighterDeathEvent != null) fighterDeathEvent(parentfighter);

        //FIX ME
        //IF NOT A HERO THAN DESTROY FIGHTER ELSE JUST DISABLE (SIMPLEST WAY TO SAVE HERO DATA)
        if (parentfighter.hero == null)
        {
            Destroy(this.gameObject);
        }
        else
        {
            parentfighter.gameObject.SetActive(false);
        }
    }

    void GetReward(Fighter killer)
    {
        if (killer.currTeam == GameManager.playerTeam)
        {
            GameManager.currGold += goldReward;

            if (killer.hero != null)
            {
                killer.hero.currXPAmount += xpReward;
            }
        }
    }


}
