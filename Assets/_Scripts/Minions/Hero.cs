﻿using UnityEngine;
using System.Collections;

public class Hero : MonoBehaviour,IMagicCaster
{
    public int currLevel = 0;
    public int maxlevel = 10;

    public int currXPAmount = 0;
    public int currSkillpoints = 0;
    private int currSkillPointsTemp = -1;

    public HeroLevel[] heroLevels = new HeroLevel[1];

    IMagic[] magicSkills = new IMagic[2];

    private ControlPanelActions controlPanel;
    private Fighter parentFighter;
    private ParticleSystem particleSystem;


	// Use this for initialization
	void Start ()
	{
	    maxlevel = heroLevels.Length;

	    particleSystem = GetComponentInChildren<ParticleSystem>();
	    controlPanel = GetComponent<ControlPanelActions>();
	    parentFighter = GetComponent<Fighter>();
        magicSkills = GetComponentsInChildren<IMagic>();

        Upgrade(0);
	}


    public void Upgrade(int changeVal)
    {
        Barrack.SetUpgradesToMinion(new Fighter[]{parentFighter}, heroLevels[currLevel].heroPropertiesOnNewLevel);
    }

    public void CancelAllMagics()
    {
        foreach (IMagic magic in magicSkills)
        {
            if (magic != null)
            {
                magic.CancelMagicAiming(null);
            }
        }
    }

    public void RemoveSkillpoint(int val)
    {
        if (currSkillpoints - val >= 0)
        {
            currSkillpoints -= val;
        }
    }

    private void FixedUpdate()
    {
        //LvlUP Check
        if (currXPAmount >= heroLevels[currLevel].xpToNextLevel && currLevel < maxlevel - 1)
        {
            currXPAmount -= heroLevels[currLevel].xpToNextLevel;
            currLevel++;
            currSkillpoints += heroLevels[currLevel].addSkillPoints;
            Upgrade(1);

            particleSystem.Emit(30);
        }


        if (currSkillpoints != currSkillPointsTemp)
        {
            controlPanel.currActionsListNumber = currSkillpoints > 0 ? 1 : 0;
            controlPanel.UpdateButtons();

            currSkillPointsTemp = currSkillpoints;
        }
    }


}
