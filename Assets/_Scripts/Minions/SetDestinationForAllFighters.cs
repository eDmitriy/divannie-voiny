﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class SetDestinationForAllFighters : MonoBehaviour
{
    public Team currTeam;

    private Fighter thisFighter;

    List<Fighter> fighters = new List<Fighter>(); 


	// Use this for initialization
	void Start ()
	{
	    thisFighter = GetComponent<Fighter>();
	    if (thisFighter == null)
	    {
	        Destroy(this);
	    }
	    StartCoroutine(Loop());
	}

    private IEnumerator Loop()
    {
        while (true)
        {
            //all enemy fighters
            fighters = GameManager.spawnedChars.Where(v => v.currTeam != currTeam && v.hp != null && v.agent != null).ToList();
            if (fighters.Count == 0)
            {
                //all friendly fighters
                fighters = GameManager.spawnedChars.Where(v => v.currTeam == currTeam && v.hp != null && v.agent != null && v.autoTarget).ToList();

                foreach (Fighter fighter in fighters)
                {
                    fighter.SetCustomTarget(transform.position, null);
                }
            }

            yield return new WaitForSeconds(0.5f);
        }
    }

}
