﻿using UnityEngine;
using System.Collections;

public class RangedAttack : Weapon_Basic,IRangedWeapon
{
    #region Variables
    public Transform projectilePrefab;
    public Transform launchPoint;
    #endregion

    
    public override void Attack(Fighter target, Vector3 movePoint)
    {
        base.Attack(target, movePoint);
        
        if (strikeCooldownOn && Vector3.Angle(transform.forward, movePoint - transform.position) < attackAngle)
        {
            return;
        }
        /*animator.speed = attackSpeed * 2f;
        animator.SetTrigger("Strike");
        animator.SetFloat("AttackN", strikeRandomN);*/

        Transform go = Instantiate(projectilePrefab, launchPoint.position, launchPoint.rotation) as Transform;
        if (go != null) go.GetComponent<Arrow>().Launch(this, target, launchPoint.position, movePoint);

        StartCoroutine(StrikeCooldown());
    }

    public void ProjectileHit(Fighter target)
    {
        if (target.hp != null)
        {
            target.hp.Damage(attackDamage, fighter);
            
        }
    }

    public override void AnimationEnd()
    {
        //Debug.Log("RangedAttack:AnimationEnd");
        strikeRandomN = Random.Range(0, 1f);
    }

}
