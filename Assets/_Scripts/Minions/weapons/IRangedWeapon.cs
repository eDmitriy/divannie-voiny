﻿
public interface IRangedWeapon
{
    void ProjectileHit(Fighter target);

}
