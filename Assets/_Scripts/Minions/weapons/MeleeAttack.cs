﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MeleeAttack : Weapon_Basic
{
    public override void Attack(Fighter target, Vector3 movePoint)
    {
        base.Attack(target, movePoint);

        if (strikeCooldownOn && Vector3.Angle(transform.forward, movePoint - transform.position) < attackAngle)
        {
            return;
        }
        animator.speed = attackSpeed * 2f;
        animator.SetTrigger("Strike");
        animator.SetFloat("AttackN", strikeRandomN);

        currTarget = target;
    }

    public override void AnimationEnd()
    {
        base.AnimationEnd();

        if (currTarget != null && currTarget.hp != null)
        {
            currTarget.hp.Damage(attackDamage, fighter);

            if (!strikeCooldownOn) StartCoroutine(StrikeCooldown());
        }
    }
}
