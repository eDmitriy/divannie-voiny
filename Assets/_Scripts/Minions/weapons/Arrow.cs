﻿using UnityEngine;
using System.Collections;

public class Arrow : MonoBehaviour
{
    private IRangedWeapon rangedAttack;
    private Vector3 startPointVector;
    private Vector3 endPointVector;
    private Fighter targetFighter;

    public float timeToFly = 1;
    public float heightMult = 1;

    public float animSubSteps = 100;

    public float waitForDestroyTime = 20f;

    private float currT = 0;
    


    public void Launch(IRangedWeapon ranged, Fighter target, Vector3 startPoint, Vector3 endPoint)
    {
        rangedAttack = ranged;
        startPointVector = startPoint;
        targetFighter = target;

        endPointVector = endPoint;
        if(target.agent!=null) endPointVector += targetFighter.transform.forward * targetFighter.agent.speed * timeToFly;
        
        StartCoroutine(FlyAnimation());
    }

    private IEnumerator FlyAnimation()
    {
        Vector3 posTemp = transform.position;
        Vector3 pos = transform.position;
        endPointVector.y = 0;
        while (true)
        {
            pos.y = Mathf.Sin(currT * Mathf.PI) * Vector3.Distance(startPointVector, endPointVector) / 5f * heightMult;
            pos.x = Mathf.Lerp(startPointVector.x, endPointVector.x, currT);
            pos.z = Mathf.Lerp(startPointVector.z, endPointVector.z, currT);

            transform.position = pos;
            transform.rotation = Quaternion.LookRotation(transform.position-posTemp);

            posTemp = transform.position;

            currT += 1f / animSubSteps / timeToFly * (1.5f - Mathf.PingPong(currT, 0.5f));

            //ON ANIMATION TIME ENDS CALL WEAPON HIT SUCCESS AND DESTROY THIS SCRIPT AND OBJECT AFTER FEW SECS
            if (currT >= 1)
            {
                if (targetFighter != null)
                {
                    rangedAttack.ProjectileHit(targetFighter);

                    if (Vector3.Distance(transform.position, targetFighter.transform.position) < 1.0f)
                    {
                        transform.parent = targetFighter.transform;
                        transform.localScale = Vector3.one;
                    }
                }
                Destroy(this.gameObject,waitForDestroyTime);
                Destroy(this);
            }

            yield return new WaitForSeconds((timeToFly/animSubSteps)/2f);
        }
    }
}
