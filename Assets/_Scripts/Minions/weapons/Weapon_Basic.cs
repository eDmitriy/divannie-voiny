﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Weapon_Basic : MonoBehaviour,IWeapon {

    #region Variables
    [Range(0, 360)]
    public float attackAngle = 45f;
    [Range(0, float.MaxValue)]
    public float attackDistance = 3f;
    [Range(0, 100)]
    public float attackSpeed = 1f;  // n/sec
    [Range(0, int.MaxValue)]
    public int attackDamage = 20;

    List<float> attackSpeedEffects = new List<float>();

    protected bool strikeCooldownOn = false;
    protected float strikeRandomN = 0;

    protected Animator animator;
    protected Fighter fighter;
    protected Fighter currTarget;

    public int AttackDamage
    {
        get { return attackDamage; }
        set { attackDamage = value; }
    }

    public float AttackSpeed
    {
        get { return attackSpeed; }
        set { attackSpeed = value; }
    }

    public float AttackRadius
    {
        get { return attackDistance; }
        set { attackDistance = value; }
    }

    public void AddAttackSpeedEffect(float value)
    {
        attackSpeedEffects.Add(value);
    }

    public void RemoveAttackSpeedEffect(float value)
    {
        attackSpeedEffects.Remove(value);
    }

    #endregion


    // Use this for initialization
    void Start()
    {
        animator = GetComponent<Animator>();
        fighter = GetComponent<Fighter>();

    }

    private void OnDisable()
    {
        strikeCooldownOn = false;
    }


    private float CalcAttackSpeedBonus()
    {
        float value = 0;
        foreach (float f in attackSpeedEffects)
        {
            value = Mathf.Clamp(value + f, -1, 1);
        }
        return (1f - value);
    }

    public bool InAttackRadius(Vector3 targetPosition)
    {
        return Vector3.Distance(transform.position, targetPosition) < attackDistance;
    }


    public virtual void Attack(Fighter target, Vector3 movePoint)
    {

    }

    public virtual void AnimationEnd()
    {
        //Debug.Log("MinionAttack:AnimationEnd");
        strikeRandomN = Random.Range(0, 1f);
    }

    protected IEnumerator StrikeCooldown()
    {
        strikeCooldownOn = true;

        yield return new WaitForSeconds(1f / Mathf.Clamp(attackSpeed * CalcAttackSpeedBonus(), 0.001f, 100f ));

        strikeCooldownOn = false;
    }
}
