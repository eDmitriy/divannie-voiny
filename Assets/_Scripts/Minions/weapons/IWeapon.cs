﻿using System.Collections.Generic;
using UnityEngine;

public interface IWeapon
{
    void Attack(Fighter target, Vector3 movePoint);

    bool InAttackRadius(Vector3 targetPosition);

    int AttackDamage { get; set; }
    float AttackSpeed { get; set; }
    float AttackRadius { get; set; }

    void AddAttackSpeedEffect(float value);
    void RemoveAttackSpeedEffect(float value);

}
