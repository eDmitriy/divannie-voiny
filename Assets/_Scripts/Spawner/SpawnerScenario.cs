﻿using System;
using UnityEngine;

[Serializable]
public class SpawnerScenario
{
    public Fighter fighterPrefab;
 [Range(0,500)]   public int count = 0;
    public FighterProperties fighterProperties = new FighterProperties();
}

[Serializable]
public class SpawnerScenarioMultiple
{
    public SpawnerScenario[] fighterInstance = new SpawnerScenario[0];
}
