﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Spawner : MonoBehaviour {
    public Team team = Team.team1;
    public Material minionMaterial;

    public SpawnerScenarioMultiple[] spawnFightersEveryWave = new SpawnerScenarioMultiple[0];

    public bool spawnOnUpgradesTimeStart = false;

    private GameState gameStateTemp = GameState.fail;

	// Use this for initialization
	void Start () {
	    StartCoroutine(Loop());
	}

    //CHECK GAME STATE CHANGES AND SPAWN NEW FIGHTER EVERY NEW WAVE ACCORDING TO SCENARIO
    private IEnumerator Loop()
    {
        while (true)
        {
            if (GameManager.gameState != gameStateTemp
                && GameManager.gameState == (spawnOnUpgradesTimeStart? GameState.upgradesTime : GameState.battleTime) )
            {
                int currWave = GameManager.currWave + (spawnOnUpgradesTimeStart ? 1 : 0);

                if (currWave < spawnFightersEveryWave.Length)
                {
                    SpawnerScenarioMultiple scenarioMult = spawnFightersEveryWave[currWave];

                    if (scenarioMult.fighterInstance.Length > 0)
                    {
                        foreach (SpawnerScenario spawnerScenario in scenarioMult.fighterInstance)
                        {
                            if (spawnerScenario.fighterPrefab != null && spawnerScenario.count > 0)
                            {
                                Fighter[] spawnedFighters = SpawnFighter(spawnerScenario.count, spawnerScenario.fighterPrefab.transform);
                               
                                //fix me
                                yield return 1;

                                Barrack.SetUpgradesToMinion(spawnedFighters, spawnerScenario.fighterProperties);
                            }
                        }
                    }


                }
            }

            if (GameManager.gameState != gameStateTemp)
            {
                gameStateTemp = GameManager.gameState;
            }

            yield return new WaitForSeconds(0.3f);
        }
    }

    public Fighter[] SpawnFighter(int count, Transform prefab)
    {
        List<Fighter> spawnedFighters = new List<Fighter>();
        Vector3 pos = transform.position;

        for (int i = 0; i < count; i++)
        {
            pos = SpawnPos();

            Transform go = Instantiate(prefab, pos, Quaternion.identity) as Transform;
            if (go != null)
            {
                go.name += " "+ team.ToString() + " " + Random.Range(0, 9999);

                Fighter fgo = go.GetComponent<Fighter>();
                fgo.currTeam = team;
                fgo.spawnerObj = transform.root.gameObject;
                GameManager.spawnedChars.Add(fgo);
                spawnedFighters.Add(fgo);

                if (minionMaterial != null) go.GetComponent<Renderer>().sharedMaterial = minionMaterial;
            }
        }
        return spawnedFighters.ToArray();
    }

    public Vector3 SpawnPos()
    {
       return transform.right * Random.Range(-transform.lossyScale.x / 2, transform.lossyScale.x / 2) +
                  transform.forward * Random.Range(-transform.lossyScale.z / 2, transform.lossyScale.z / 2) +
                  transform.position;
    }

}
