﻿using System;
using UnityEngine;
using System.Collections;

/*[Serializable]*/
public class CustomTarget
{
    public Fighter fighterTarget;
    public Vector3 movePosition;

    public bool isReached = true;
    public bool fighterTargetIsSetted = false;

    public bool TargetIsReached(Vector3 parentPosition, float reachDistance)
    {
        //EXIT CONDITIONS FOR MANUAL TARGET
        //1 IF ONLY POSITION WAS SET CHECK ONLY DISTANCE
        //2 IF TARGET FIGHTER WAS SET THEN WAITING FOR ITS DEATH
        if ((Vector3.Distance(parentPosition, movePosition) < reachDistance && fighterTarget == null)
            || (fighterTargetIsSetted && fighterTarget == null)
            )
        {
            isReached = true;
            return true;
        }
        return false;
    }

    public void NewTarget(Fighter fighter, Vector3 movPos)
    {
        if (fighter != null)
        {
            fighterTargetIsSetted = true;
        }
        fighterTarget = fighter;

        movePosition = movPos;

        isReached = true;
    }

    public Vector3 TargetPosition()
    {
        if (fighterTarget == null)
        {
            return movePosition;
        }
        return fighterTarget.transform.position;
    }

}
