﻿using System;
using UnityEngine;
using System.Collections;

[Serializable]
public class IceBoltUpgradeLevel
{
    [Header("Spell Properties")]
[Range(0, 100)] public float castRadius = 100f;
[Range(0, 9000)] public int spellDamage = 50;

[Range(0, 100)] public float coolDownTime = 5f;

    [Header("Effects On Hit Properties")]
[Range(-1, 1)] public float additionalEffectPower = 0.1f;
[Range(0.1f, 100)] public float additionalEffectDuration = 3f;

}
