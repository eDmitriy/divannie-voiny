﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class IceBolt : Magic_Basic,IRangedWeapon {

    #region Variables
    [Header("Projectile")]
    public Transform projectilePrefab;
    public Transform launchPoint;

    [Header("Ice Bolt Upgrades")]
    public IceBoltUpgradeLevel[] iceBoltUpgradeLevels = new IceBoltUpgradeLevel[5];
    
    #endregion


    private void OnEnable()
    {
        InputManager.leftMouse += SelectTarget;
        InputManager.leftMouseBreak += RightMouseClick;
        InputManager.rightMouse += RightMouseClick;
    }
    private void OnDisable()
    {
        InputManager.leftMouse -= SelectTarget;
        InputManager.leftMouseBreak -= RightMouseClick;
        InputManager.rightMouse -= RightMouseClick;
    }


    private void Awake()
    {
        maxLevel = iceBoltUpgradeLevels.Length;
        Upgrade(0);

    }


    void Update()
    {
        if (Input.GetKeyDown(hotkey) && IsSelected())
        {
            StartMagicAiming();
        }
        if (Input.GetKeyDown(KeyCode.Escape) && IsSelected())
        {
            CancelMagicAiming(null);
        }
    }

    public void Upgrade(int changeVal)
    {
        if ( (currLevel + changeVal) < 0 || (currLevel + changeVal) > (maxLevel-1) )
        {
            return;
        }
        if (magicCaster != null) magicCaster.RemoveSkillpoint(1);
        currLevel = Mathf.Clamp(currLevel + changeVal, 0, iceBoltUpgradeLevels.Length);

        castRadius = iceBoltUpgradeLevels[currLevel].castRadius;
        coolDownTime = iceBoltUpgradeLevels[currLevel].coolDownTime;
        spellDamage = iceBoltUpgradeLevels[currLevel].spellDamage;
    }

    public override void Cast(Fighter target, Vector3 movePoint)
    {
        Transform go = Instantiate(projectilePrefab, launchPoint.position, launchPoint.rotation) as Transform;
        if(go!=null) go.GetComponent<Arrow>().Launch(this, target, launchPoint.position, movePoint);

        base.Cast(target, movePoint);
    }


    public void ProjectileHit(Fighter target)
    {
        if (target.hp != null)
        {
            target.hp.Damage(iceBoltUpgradeLevels[currLevel].spellDamage, parentFighter);

            foreach (IMagicEffect effect in magicEffects)
            {
                effect.CreateCopyOnObjectAndInitialize(
                    target.gameObject, 
                    iceBoltUpgradeLevels[currLevel].additionalEffectDuration,
                    iceBoltUpgradeLevels[currLevel].additionalEffectPower
                    );
            }
        }
    }


}
