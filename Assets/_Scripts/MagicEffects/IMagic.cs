﻿
using System;
using UnityEngine.EventSystems;

public enum MagicState
{
    idle,
    aiming,
    targetSelected,
    casting,
    cooldown
}

public interface IMagic
{
    void CancelMagicAiming(PointerEventData eventData);

}
