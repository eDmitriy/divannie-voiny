﻿using System;
using UnityEngine;
using System.Collections;

[Serializable]
public class MeteorShowerUpgradeLevel  {

    [Header("Spell Properties")]
    [Range(5, 1000)] public float castRadius = 100f;
    [Range(0, 100)] public float spellDamageRadius = 30f;
    [Range(0, 100)] public float coolDownTime = 5f;
    [Range(0.1f, 100)] public float effectDuration = 2f;

    [Range(0, 100)] public int spellDamage = 50;

    [Header("Effects On Hit Properties")]
    [Range(-1, 1)] public float additionalEffectPower = 0.1f;
    [Range(0, 100)] public float additionalEffectDuration = 3f;
}
