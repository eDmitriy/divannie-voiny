﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.EventSystems;

public class MeteorShower : Magic_Basic
{
    [Header("Effects")]
    public Transform particleEffectPrefab;
    public Transform aimingHelperPrefab;

    [Header("Meteor Shower Upgrades")]
    public MeteorShowerUpgradeLevel[] meteorShowerUpgradeLevels = new MeteorShowerUpgradeLevel[3];

    //additional spell params
    private float spellDamageRadius = 30f;
    private float effectDuration = 2f;

    //
    private Transform aimingHelperObj;
    private Transform particleEffectObj;

    private RaycastHit hit;
    private Ray ray;
    private Camera mainCamera;


    private void OnEnable()
    {
        InputManager.leftMouse += SelectTarget;
        InputManager.leftMouseBreak += SelectPosition;
        InputManager.rightMouse += RightMouseClick;
    }
    private void OnDisable()
    {
        InputManager.leftMouse -= SelectTarget;
        InputManager.leftMouseBreak -= SelectPosition;
        InputManager.rightMouse -= RightMouseClick;

        if (aimingHelperObj != null)
        {
            Destroy(aimingHelperObj.gameObject);
        }
        if (particleEffectObj != null) Destroy(particleEffectObj.gameObject);
        
        CancelMagicAiming(null);
    }

    private void Awake()
    {
        maxLevel = meteorShowerUpgradeLevels.Length;
        mainCamera = Camera.main;

        Upgrade(0);

    }


    void Update()
    {
        if (Input.GetKeyDown(hotkey) && IsSelected())
        {
            StartMagicAiming();
        }
        if (Input.GetKeyDown(KeyCode.Escape) && IsSelected())
        {
            CancelMagicAiming(null);
        }

        AimingHelper();
    }
    

    private void AimingHelper()
    {
        if (currMagicState == MagicState.aiming && IsSelected())
        {
            if (aimingHelperObj == null)
            {
                aimingHelperObj = Instantiate(aimingHelperPrefab) as Transform;
                if (aimingHelperObj.GetComponent<Projector>() != null)
                {
                    aimingHelperObj.GetComponent<Projector>().orthographicSize = spellDamageRadius * 1.2f;
                }
            }
            else
            {
                ray = mainCamera.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(ray, out hit, 5000))
                {
                    aimingHelperObj.transform.position = hit.point + Vector3.up * 100f;
                }

            }
        }
        else
        {
            if (aimingHelperObj != null) Destroy(aimingHelperObj.gameObject);
            
        }
    }

    public void Upgrade(int changeVal)
    {
        if ((currLevel + changeVal) < 0 || (currLevel + changeVal) > (maxLevel - 1)
            )
        {
            return;
        }
        if (magicCaster != null) magicCaster.RemoveSkillpoint(1);
        currLevel = Mathf.Clamp(currLevel + changeVal, 0, meteorShowerUpgradeLevels.Length);

        castRadius = meteorShowerUpgradeLevels[currLevel].castRadius;
        coolDownTime = meteorShowerUpgradeLevels[currLevel].coolDownTime;
        spellDamage = meteorShowerUpgradeLevels[currLevel].spellDamage;
        spellDamageRadius = meteorShowerUpgradeLevels[currLevel].spellDamageRadius;

    }

    public override void Cast(Fighter target, Vector3 movePoint)
    {
        StartCoroutine(MagicAnimation(movePoint));

        base.Cast(target, movePoint);
    }

    private IEnumerator MagicAnimation(Vector3 movePoint)
    {
        //Vector3 startPoint = movePoint + Vector3.up * 30;

        particleEffectObj = Instantiate(particleEffectPrefab, movePoint, Quaternion.identity) as Transform;
        if (particleEffectObj != null)
        {
            var particleSystem = particleEffectObj.GetComponentInChildren<ParticleSystem>();

            var shape = particleSystem.shape;
            shape.radius = spellDamageRadius;

            particleSystem.Play();
            
            yield return new WaitForSeconds(effectDuration / 2);

            //find all enemy fighters in spellDamage radius
            List<Fighter> fightersInSpellDamageRadius =
                GameManager.spawnedChars.Where(v => v.currTeam != parentFighter.currTeam && v.hp != null && v.agent != null
                && Vector3.Distance(v.transform.position, movePoint) < spellDamageRadius).ToList();

            foreach (Fighter fighter in fightersInSpellDamageRadius)
            {
                DoDamage(fighter);
                foreach (IMagicEffect effect in magicEffects)
                {
                    effect.CreateCopyOnObjectAndInitialize(
                        fighter.gameObject,
                        meteorShowerUpgradeLevels[currLevel].additionalEffectDuration,
                        meteorShowerUpgradeLevels[currLevel].additionalEffectPower
                        );
                }
            }

            yield return new WaitForSeconds(effectDuration / 2);
            if (particleSystem!=null) particleSystem.Stop();

            if (particleEffectObj != null) Destroy(particleEffectObj.gameObject, 10);
            particleEffectObj = null;
        }

        yield return null;
    }

}
