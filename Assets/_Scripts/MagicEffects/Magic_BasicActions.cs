﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class Magic_Basic : MonoBehaviour, IMagic, IControlPanelButtonInfo {

    #region Variables
    protected MagicState currMagicState;

    public KeyCode hotkey = KeyCode.S;
    public Texture2D aimingCursor;

    [Header("Upgrades")]
    public int currLevel = 0;
    protected int maxLevel = 5;

    protected float currCooldown = 0f;

    //spell params
    protected float coolDownTime = 0;
    protected float castRadius = 30;
    protected int spellDamage = 50;

    [Header("Add Magi effects with damage")]
    protected IMagicEffect[] magicEffects = new IMagicEffect[0];

    protected Fighter parentFighter;
    protected IMagicCaster magicCaster;

    protected CustomTarget customTarget;

    protected const float cooldownUpdateSubsteps = 20;
    #endregion

/*
    private void OnEnable()
    {
        StartCoroutine(CooldownUpdateLoop());
        StartCoroutine(TryCastLoop());
    }*/

    void Start()
    {
        parentFighter = transform.root.GetComponentInChildren<Fighter>();
        magicCaster = parentFighter.GetComponentInChildren<IMagicCaster>();
        magicEffects = GetComponents<IMagicEffect>();
    }

    private void FixedUpdate()
    {
        if (GameManager.gameState == GameState.pause) return;

        TryCast();
        CooldownUpdate();
    }

    #region Logic

    public void StartMagicAiming()
    {
        if (!IsSelected()) return;
        
        if (currMagicState != MagicState.idle || parentFighter == null || magicCaster==null) return;

        magicCaster.CancelAllMagics();

        InputManager.leftMouseAction = LeftMouseAction.selectMagickTarget;

        currMagicState = MagicState.aiming;

        if (aimingCursor != null)
        {
            Vector2 cursorHotSpot = new Vector2(aimingCursor.width / 2f, aimingCursor.height / 2f);
            Cursor.SetCursor(aimingCursor, cursorHotSpot, CursorMode.Auto);
        }
    }

    public void RightMouseClick(PointerEventData eventData)
    {
        if (IsSelected() && (currMagicState == MagicState.aiming || currMagicState == MagicState.targetSelected))
        {
            CancelMagicAiming(null);
        }
    }

    public void CancelMagicAiming(PointerEventData eventData)
    {
        currMagicState = MagicState.idle;
        customTarget = null;

        InputManager.leftMouseAction = LeftMouseAction.selectTarget;

        Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
    }

    public void SelectTarget(Selectable selectable)
    {
        if (IsSelected()
            && currMagicState == MagicState.aiming
            && selectable.fighter != null
            && parentFighter.currTeam != selectable.fighter.currTeam
            )
        {
            customTarget = new CustomTarget();
            customTarget.NewTarget(selectable.fighter, Vector3.zero);

            parentFighter.SetCustomTarget(customTarget.fighterTarget.transform.position, customTarget.fighterTarget);

            currMagicState = MagicState.targetSelected;

            InputManager.leftMouseAction = LeftMouseAction.selectTarget;
        }
        else
        {
            CancelMagicAiming(null);
        }
        Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
    }

    public void SelectPosition(PointerEventData eventData)
    {
        if (IsSelected() && currMagicState == MagicState.aiming)
        {
            customTarget = new CustomTarget();
            customTarget.NewTarget(null, eventData.pointerCurrentRaycast.worldPosition);

            parentFighter.SetCustomTarget(customTarget.movePosition, null);

            currMagicState = MagicState.targetSelected;

            InputManager.leftMouseAction = LeftMouseAction.selectTarget;
        }
        else
        {
            CancelMagicAiming(null);
        }
        Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
    }

    public virtual void Cast(Fighter target, Vector3 movePoint)
    {
        currCooldown += coolDownTime;
        currMagicState = MagicState.cooldown;
        parentFighter.CancelCustomTarget();

        customTarget = null;
    }

    public void DoDamage(Fighter target)
    {
        if (target.hp != null)
        {
            target.hp.Damage(spellDamage, parentFighter);
        }
    }


    private void TryCast()
    {
        if (customTarget == null && currMagicState == MagicState.targetSelected)
        {
            CancelMagicAiming(null);
        }

        if (currMagicState == MagicState.targetSelected && customTarget != null &&
            InAttackRadius(customTarget.TargetPosition()))
        {
            //Cast(castTargetFighter, castTargetFighter.transform.position);
            Cast(customTarget.fighterTarget, customTarget.TargetPosition());
        }
    }

    private void CooldownUpdate()
    {
        currCooldown = Mathf.Clamp(currCooldown - (Time.fixedDeltaTime/coolDownTime) /*cooldownUpdateSubsteps*/, 0f, 1);

        if (Mathf.Approximately(0, currCooldown) && currMagicState == MagicState.cooldown)
        {
            currMagicState = MagicState.idle;
        }

        //yield return new WaitForSeconds(coolDownTime / cooldownUpdateSubsteps);
    }

    bool InAttackRadius(Vector3 targetPosition)
    {
        return Vector3.Distance(transform.position, targetPosition) < castRadius;
    }

    protected bool IsSelected()
    {
        if (parentFighter.GetComponent<Selectable>() != null)
        {
            return parentFighter.GetComponent<Selectable>().isSelected;
        }
        return false;
    }
    public void AnimationEnd()
    {
        //Debug.Log("RangedAttack:AnimationEnd");
    }
    #endregion

    #region IControlPanelButtonInfo
    public int Cost
    {
        get { return 0; }
    }

    public float Cooldown
    {
        get { return currCooldown; }
    }

    public int OrderCount
    {
        get { return currLevel+1; }
    }

    public bool IsUpgradeOnMaxLevel
    {
        get { return currLevel < (maxLevel - 1); }
    }

    #endregion

    
}
