﻿using UnityEngine;
using System.Collections;

public interface IMagicEffect
{
    void Initialize(float time, float power);

    void CreateCopyOnObjectAndInitialize(GameObject targetGO, float time, float power);


}
