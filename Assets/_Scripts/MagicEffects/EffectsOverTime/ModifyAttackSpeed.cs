﻿using UnityEngine;
using System.Collections;

public class ModifyAttackSpeed : MonoBehaviour,IMagicEffect {

    public float duration = 0;
    public float effectPower = 0.5f; //0- no change; 1-cant attack

    public bool isActive;

    private Fighter fighter;


    // Update is called once per frame
    void FixedUpdate()
    {
        if (!isActive) return;
        if (GameManager.gameState == GameState.pause) return;

        duration -= Time.fixedDeltaTime;

        if (duration <= 0)
        {
            CancelEffect();
        }
    }

    private void OnDisable()
    {
        CancelEffect();
    }

    public void CreateCopyOnObjectAndInitialize(GameObject targetGO, float time, float power)
    {
        IMagicEffect effect = targetGO.AddComponent<ModifyAttackSpeed>();
        effect.Initialize(time, power);
    }

    public void Initialize(float time, float power)
    {
        duration += time;
        effectPower = power;

        fighter = GetComponent<Fighter>();
        if (fighter != null && fighter.weapon != null)
        {
            fighter.weapon.AddAttackSpeedEffect(effectPower);
            isActive = true;
        }
        else
        {
            CancelEffect();
        }
    }

    private void CancelEffect()
    {
        isActive = false;

        if (fighter != null && fighter.weapon != null)
        {
            fighter.weapon.RemoveAttackSpeedEffect(effectPower);
        }
        Destroy(this);
    }
}
