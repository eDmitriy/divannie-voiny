﻿
public interface IMagicCaster
{
    void CancelAllMagics();

    void RemoveSkillpoint(int val);
}
