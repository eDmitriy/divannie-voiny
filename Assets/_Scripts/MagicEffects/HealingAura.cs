﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class HealingAura : MonoBehaviour
{
    public Team currTeam;
    
    public float healDistance = 30f;
    public float ticksPerSec = 10;

    public int minionHealVal = 10;
    public int heroHealVal = 20;

    List<Fighter> friendlyFighters = new List<Fighter>(100);

    public ParticleSystem healEffectParticleSystem;


	// Use this for initialization
	void Start ()
	{
	    StartCoroutine(Loop());
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    private IEnumerator Loop()
    {
        Fighter fighter;
        while (true)
        {
            //HEAL ONLY FRIENDLY TEAMMATES WITH HEALTH AND MOVABLE
            friendlyFighters =
                GameManager.spawnedChars.Where(v => v.currTeam == currTeam && v.hp != null && v.agent!=null)
                    .ToList();

            for (int i = 0; i < friendlyFighters.Count; i++)
            {
                fighter = friendlyFighters[i];
                if (fighter != null)
                {
                    if (Vector3.Distance(transform.position, fighter.transform.position) < healDistance)
                    {
                        int healVal = fighter.hero!=null ? heroHealVal : minionHealVal;
                        healVal /= (int)ticksPerSec;

                        fighter.hp.Heal(healVal);

                        ParticleEffect(fighter.transform.position);
                    }
                }
            }

            yield return new WaitForSeconds(1/ticksPerSec);
        }
    }

    private void ParticleEffect(Vector3 position)
    {
        if (healEffectParticleSystem != null) healEffectParticleSystem.Emit(
           new ParticleSystem.EmitParams()
           {
               position = position,
               startLifetime = 0.5f,
               velocity = Vector3.up * 2
           },
           10
        );
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.white;
        Gizmos.DrawWireSphere(transform.position, healDistance);
    }
}
